package conf;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import entity.StatusTest;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import property.PropertyService;

import javax.persistence.EntityManagerFactory;
import java.util.HashMap;
import java.util.Map;


public class Configuration extends AbstractModule {

    @Singleton
    @Provides
    public PropertyService propertyService() {
        return new PropertyService();
    }

    @Singleton
    private Map<String, String> settings() {
        final Map<String, String> settings = new HashMap<>();
        settings.put(Environment.DRIVER, propertyService().getJdbcDriver());
        settings.put(Environment.URL, propertyService().getJdbcUrl());
        settings.put(Environment.USER, propertyService().getJdbcUsername());
        settings.put(Environment.PASS, propertyService().getJdbcPassword());
        settings.put(Environment.DIALECT, "org.hibernate.dialect.MySQL5InnoDBDialect");
        settings.put(Environment.HBM2DDL_AUTO, "create");
        settings.put(Environment.SHOW_SQL, "true");
        return settings;
    }

    @Singleton
    private StandardServiceRegistryBuilder registryBuilder() {
        StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings());
        return registryBuilder;
    }

    @Singleton
    private StandardServiceRegistry registry() {
        return registryBuilder().build();
    }

    @Singleton
    @Provides
    final private EntityManagerFactory managerFactory() {
        MetadataSources sources = new MetadataSources(registry());
        sources.addAnnotatedClass(StatusTest.class);
        final Metadata metadata = sources.getMetadataBuilder().build();
        EntityManagerFactory managerFactory = metadata.getSessionFactoryBuilder().build();
        return managerFactory;
    }

    @Override
    protected void configure() {;
    }

}