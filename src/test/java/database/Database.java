package database;

import entity.StatusTest;
import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

public class Database {

    private final EntityManagerFactory ems;

    public Database(EntityManagerFactory ems) {
        this.ems = ems;
    }

    public void merge(@NotNull final StatusTest step) {
        EntityManager em = ems.createEntityManager();
        try {
            em.getTransaction().begin();
            em.merge(step);
            em.getTransaction().commit();
        } catch (RuntimeException e) {
            em.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            em.close();
        }
    }

}