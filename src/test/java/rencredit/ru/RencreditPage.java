package rencredit.ru;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class RencreditPage {

    private WebDriver driver;

    public RencreditPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = "/html/body/div[4]/div[1]/div[2]/div/div[2]/div[3]/div[1]/a[1]")
    private WebElement contribution;

    @FindBy(xpath = "/html/body/div[4]/div[1]/div[2]/div/div[2]/div[1]/div[1]/a[1]")
    private WebElement cart;

    public void clickContribution() {
        contribution.click();
    }

    public void clickCart() {
        cart.click();
    }

}