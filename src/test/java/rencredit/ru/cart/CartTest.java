package rencredit.ru.cart;

import com.google.inject.Inject;
import conf.Configuration;
import entity.StatusTest;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Guice;
import org.testng.annotations.Test;
import property.PropertyService;
import rencredit.ru.RencreditPage;
import database.Database;

import javax.persistence.EntityManagerFactory;
import java.util.Date;
import java.util.concurrent.TimeUnit;

@Guice(modules = {Configuration.class})
public class CartTest {

    private static RencreditPage rencreditPage;

    private static CartPage cartPage;

    private static WebDriver webDriver;

    private static PropertyService propertyService;

    private static StatusTest step;

    private static EntityManagerFactory ems;

    @Inject
    public CartTest(
            PropertyService propertyService,
            EntityManagerFactory ems
    ) {
        this.propertyService = propertyService;
        this.ems = ems;
    }

    @BeforeClass
    public static void setup() {
        System.setProperty("webdriver.chrome.driver", propertyService.getChromeDrive());
        webDriver = new ChromeDriver();
        webDriver.manage().window().maximize();
        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        step = new StatusTest();
        step.setNameStep("Открытие страницы \"rencredit.ru\"");
        step.setBeginStep(new Date());
        webDriver.get(propertyService.getRencreditUrl());

        rencreditPage = new RencreditPage(webDriver);
        cartPage = new CartPage(webDriver);
    }

    @Test
    public void secondTest() {
        step.setEndStep(new Date());
        StatusTest step1 = new StatusTest();
        step1.setNameStep("Открытие страницы \"rencredit.ru/cards/\"");
        step1.setBeginStep(new Date());
        rencreditPage.clickCart();
        Assert.assertEquals(cartPage.getUrlCart(), webDriver.getCurrentUrl());
        step1.setEndStep(new Date());

        StatusTest step2 = new StatusTest();
        step2.setNameStep("Нажатие кнопки \"заполнить заявку на карту\"");
        step2.setBeginStep(new Date());
        String urlFillOutOnApplicationForCard
                = cartPage.getUrlFillOutOnApplicationForCard();
        cartPage.clickFillOutOnApplicationForCard();
        Assert.assertEquals(
                urlFillOutOnApplicationForCard,
                webDriver.getCurrentUrl()
        );
        step2.setEndStep(new Date());

        StatusTest step3 = new StatusTest();
        step3.setNameStep("Заполнение поля \"Фамилия\"");
        step3.setBeginStep(new Date());
        cartPage.fillSecondName("Петров");
        Assert.assertEquals("Петров", cartPage.getFirstNAme());
        step3.setEndStep(new Date());

        StatusTest step4 = new StatusTest();
        step4.setNameStep("Заполнение поля \"Имя\"");
        step4.setBeginStep(new Date());
        cartPage.fillFirstName("Никита");
        Assert.assertEquals("Никита", cartPage.getSecondName());
        step4.setEndStep(new Date());

        StatusTest step5 = new StatusTest();
        step5.setNameStep("Заполнение поля \"Номер телефона\"");
        step5.setBeginStep(new Date());
        cartPage.fillNumberTel("9001111111");
        Assert.assertEquals("+7 (900) 111-11-11", cartPage.getNumberTel());
        step5.setEndStep(new Date());

        StatusTest step6 = new StatusTest();
        step6.setNameStep("Заполнение поля \"E-mail\"");
        step6.setBeginStep(new Date());
        cartPage.fillEmail("vlad@vlad.ru");
        Assert.assertEquals("vlad@vlad.ru", cartPage.getEmail());
        step6.setEndStep(new Date());

        StatusTest step7 = new StatusTest();
        step7.setNameStep("Выбор чекбокса \"Нет отчества\"");
        step7.setBeginStep(new Date());
        cartPage.clickCheckBoxNotLastName();
        Assert.assertEquals("true", cartPage.disabledLastName());
        step7.setEndStep(new Date());

        StatusTest step8 = new StatusTest();
        step8.setNameStep("Выбор значения из выпадающего списка \"Где вы желаете получить карту\"");
        step8.setBeginStep(new Date());
        cartPage.choosePickUpCard("Пензенская область", "Пенза");
        Assert.assertEquals("Пенза", cartPage.getSelectCity());
        Assert.assertEquals("Пензенская область", cartPage.getSelectRegion());
        step8.setEndStep(new Date());

        StatusTest step9 = new StatusTest();
        step9.setNameStep("Выбр пола");
        step9.setBeginStep(new Date());
        cartPage.clickGender();
        step9.setEndStep(new Date());

        StatusTest step10 = new StatusTest();
        step10.setNameStep("Нажатие кнопки далее");
        step10.setBeginStep(new Date());
        cartPage.clickFurther();
        Assert.assertEquals(cartPage.getUrlStepTwoFill(), webDriver.getCurrentUrl());
        step10.setEndStep(new Date());

        Database database = new Database(ems);
        database.merge(step);
        database.merge(step1);
        database.merge(step2);
        database.merge(step3);
        database.merge(step4);
        database.merge(step5);
        database.merge(step6);
        database.merge(step7);
        database.merge(step8);
        database.merge(step9);
        database.merge(step10);
    }

    @AfterSuite
    public static void exit() {
        webDriver.quit();
    }

}