package rencredit.ru.cart;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CartPage {

    final private WebDriver driver;

    @FindBy(xpath = "/html/body/div[4]/div/div[2]/div[3]/div/div[2]/a")
    private WebElement fillOutOnApplicationForCard;

    @FindBy(xpath = "//*[@id=\"t1\"]")
    private WebElement firstName;

    @FindBy(xpath = "//*[@id=\"t2\"]")
    private WebElement secondName;

    @FindBy(xpath = "//*[@id=\"t3\"]")
    private WebElement lastName;

    @FindBy(xpath = "//*[@id=\"section_1\"]/div[2]/div/form/div[1]/div[4]/div/label/div")
    private WebElement checkBoxNotLastName;

    @FindBy(xpath = "//*[@id=\"s2-styler\"]")
    private WebElement pickUpTheCardRegion;

    @FindBy(xpath = "//*[@id=\"s3-styler\"]")
    private WebElement pickUpTheCardCity;

    @FindBy(xpath = "//*[@id=\"t4\"]")
    private WebElement numberTel;

    @FindBy(xpath = "//*[@id=\"t38\"]")
    private WebElement email;

    @FindBy(xpath = "//*[@id=\"section_1\"]/div[2]/div/form/div[1]/div[11]/div[3]/div/a")
    private WebElement further;

    @FindBy(xpath = "//*[@id=\"section_1\"]/div[2]/div/form/div[1]/div[5]/div[2]")
    private WebElement gender;

    public CartPage(final WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    public void clickFillOutOnApplicationForCard() {
        fillOutOnApplicationForCard.click();
    }

    public String getUrlFillOutOnApplicationForCard() {
        return "https://rencredit.ru/app/card/auto";
    }

    public void clickFurther() {
        further.click();
    }

    public void clickGender() {
        gender.click();
    }

    public String getUrlStepTwoFill() {
        WebDriverWait driverWait = new WebDriverWait(driver, 15);
        driverWait.until(
                ExpectedConditions.visibilityOfElementLocated(By.id("t8"))
        );
        return "https://rencredit.ru/app/card/auto/move_2/";
    }

    public String getUrlCart() {
        return "https://rencredit.ru/cards/";
    }

    public void fillSecondName(final String name) {
        firstName.sendKeys(name);
    }

    public void fillFirstName(final String name) {
        secondName.sendKeys(name);
    }

    public void fillNumberTel(final String tel) {
        numberTel.sendKeys(tel);
    }

    public String getNumberTel() {
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions
                .visibilityOf(numberTel));
        return numberTel.getAttribute("value");
    }

    public void fillEmail(final String emailParam) {
        email.sendKeys(emailParam);
    }

    public String getEmail() {
        return email.getAttribute("value");
    }

    public String getSecondName() {
        return secondName.getAttribute("value");
    }

    public String getFirstNAme() {
        return firstName.getAttribute("value");
    }

    public void clickCheckBoxNotLastName() {
        checkBoxNotLastName.click();
    }

    public String disabledLastName() {
        return lastName.getAttribute("disabled");
    }

    public void choosePickUpCard(final String region, final String city) {
        Select selectRegion = new Select(driver.findElement(By.xpath("//*[@id=\"s2\"]")));
        selectRegion.selectByVisibleText(region);
        Select selectCity = new Select(driver.findElement(By.xpath("//*[@id=\"s3\"]")));
        selectCity.selectByVisibleText(city);
    }

    public String getSelectRegion() {
        WebElement getAttributeSelectRegion
                = driver.findElement(By.xpath("//*[@id=\"s2-styler\"]/div[1]/div[1]"));
        return getAttributeSelectRegion.getText();
    }

    public String getSelectCity() {
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions
                .visibilityOfElementLocated(By.xpath("//*[@id=\"s3-styler\"]/div[1]/div[1]")));
        WebElement getAttributeSelectCity
                = driver.findElement(By.xpath("//*[@id=\"s3-styler\"]/div[1]/div[1]"));
        return getAttributeSelectCity.getText();
    }

}