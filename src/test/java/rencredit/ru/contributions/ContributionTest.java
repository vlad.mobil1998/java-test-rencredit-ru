package rencredit.ru.contributions;

import com.google.inject.Inject;
import conf.Configuration;
import database.Database;
import entity.StatusTest;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Guice;
import org.testng.annotations.Test;
import property.PropertyService;
import rencredit.ru.RencreditPage;

import javax.persistence.EntityManagerFactory;
import java.io.File;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import static com.codeborne.selenide.Selenide.download;

@Guice(modules = {Configuration.class})
public class ContributionTest {

    private static RencreditPage rencreditPage;

    private static EntityManagerFactory ems;

    private static ContributionsPage contributionsPage;

    private static WebDriver webDriver;

    private static PropertyService propertyService;

    private static StatusTest step;

    @Inject
    public ContributionTest(
            EntityManagerFactory ems,
            PropertyService propertyService
    ) {
        this.ems = ems;
        this.propertyService = propertyService;
    }

    @BeforeSuite
    public static void setup() {
        System.setProperty("webdriver.chrome.driver", propertyService.getChromeDrive());
        webDriver = new ChromeDriver();
        webDriver.manage().window().maximize();
        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        step = new StatusTest();
        step.setNameStep("Открытие страницы rencredit.ru");
        step.setBeginStep(new Date());
        webDriver.get(propertyService.getRencreditUrl());
        rencreditPage = new RencreditPage(webDriver);
        contributionsPage = new ContributionsPage(webDriver);
    }

    @Test
    public void firstTest() throws java.io.IOException, java.net.URISyntaxException {
        final StatusTest step1 = new StatusTest();
        step.setEndStep(new Date());
        step1.setNameStep("Нажатие кнопки вклады. Переход на страницу \"rencredit.ru/contributions/\"");
        step1.setBeginStep(new Date());
        rencreditPage.clickContribution();
        Assert.assertEquals(
                contributionsPage.getUrlContribution(),
                webDriver.getCurrentUrl()
        );
        step1.setEndStep(new Date());

        final StatusTest step2 = new StatusTest();
        step2.setNameStep("Выбор чекбокса \"В отделении банка\"");
        step2.setBeginStep(new Date());
        contributionsPage.clickCheckBoxOpenContribution();
        Assert.assertTrue(contributionsPage.checkedCheckBox());
        step2.setEndStep(new Date());

        StatusTest step3 = new StatusTest();
        step3.setNameStep("Ввод суммы вклада");
        step3.setBeginStep(new Date());
        contributionsPage.inputDepositAmount("178889");
        Assert.assertEquals("178 889", contributionsPage.getInputDepositAmount());
        step3.setEndStep(new Date());

        StatusTest step4 = new StatusTest();
        step4.setNameStep("Сдвиг ползунка \"На срок\"");
        step4.setBeginStep(new Date());
        contributionsPage.moveSlider();
        Assert.assertEquals("13", contributionsPage.getValueSelectTextBox());
        step4.setEndStep(new Date());
        StatusTest step5 = new StatusTest();
        step5.setNameStep("Скачивание \"Общие условия открытия и обслуживания банковских счетов\"");
        step5.setBeginStep(new Date());
        File file = download("https://rencredit.ru/upload/iblock/ac3/9_27_20.pdf");
        step5.setEndStep(new Date());

        Database database = new Database(ems);
        database.merge(step);
        database.merge(step1);
        database.merge(step2);
        database.merge(step3);
        database.merge(step4);
        database.merge(step5);
    }

    @AfterSuite
    public static void exit() {
        webDriver.quit();
    }

}