package rencredit.ru.contributions;

import org.jetbrains.annotations.NotNull;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ContributionsPage {

    @NotNull
    final private WebDriver driver;

    @FindBy(xpath = "//*[@id=\"section_1\"]/div/div/form/div[1]/div[2]/div/div[1]/label/span[1]/div")
    private WebElement checkBoxOpenContribution;

    @FindBy(xpath = "//*[@id=\"section_1\"]/div/div/form/div[1]/div[3]/div[1]/div/label/input")
    private WebElement textBoxDepositAmount;

    @FindBy(xpath = "//*[@id=\"section_1\"]/div/div/form/div[1]/div[4]/div[3]/div[2]/div[5]")
    private WebElement sliderPeriod;

    @FindBy(xpath = "//*[@id=\"section_1\"]/div/div/form/div[1]/div[3]/div[1]/div/label/input")
    private WebElement valueTextBoxDepositAmount;

    @FindBy(xpath = "//*[@id=\"period\"]")
    private WebElement valueSelectTextBox;

    @FindBy(xpath = "//*[@id=\"section_1\"]/div/div/form/div[1]/div[2]/div/div[1]/label/span[1]/div")
    private WebElement checkedCheckBox;

    final private String offsetValue = "80%";

    public String getUrlContribution() {
        return "https://rencredit.ru/contributions/";
    }

    public ContributionsPage(@NotNull final WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    public Boolean checkedCheckBox() {
        final String str = checkedCheckBox.getAttribute("class");
        return str.contains("checked");
    }

    public void clickCheckBoxOpenContribution() {
        checkBoxOpenContribution.click();
    }

    public void inputDepositAmount(final String deposit) {
        textBoxDepositAmount.sendKeys(deposit);
    }

    public void moveSlider() {
        sliderPeriod.click();
    }

    public String getInputDepositAmount() {
        return valueTextBoxDepositAmount.getAttribute("value");
    }

    public String getValueSelectTextBox() {
        return valueSelectTextBox.getAttribute("value");
    }

}