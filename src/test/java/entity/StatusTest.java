package entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;
import java.util.UUID;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class StatusTest {

    @Id
    private String id = UUID.randomUUID().toString();

    @Column
    private String nameStep;

    @Column
    private Date beginStep;

    @Column
    private Date endStep;

}