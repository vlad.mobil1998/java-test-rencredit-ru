package property;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyService {

    private Properties properties;

    public PropertyService() {
        try {
            final InputStream inputStream
                    = PropertyService.class.getResourceAsStream("/conf.properties");
            properties = new Properties();
            properties.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
    }

    public String getChromeDrive() {
        return properties.getProperty("chromeDrive");
    }

    public String getRencreditUrl() {
        return properties.getProperty("rencreditUrl");
    }

    public String getServerHost() {
        final String propertyHost = properties.getProperty("server.host");
        final String envHost = System.getProperty("server.host");
        if (envHost != null) return envHost;
        return propertyHost;
    }

    public Integer getServerPort() {
        final String propertyPort = properties.getProperty("server.port");
        final String envPort = System.getProperty("server.port");
        String value = propertyPort;
        if (envPort != null) value = envPort;
        return Integer.parseInt(value);
    }

    public String getJdbcDriver() {
        return properties.getProperty("db.JdbcDriver");
    }

    public String getJdbcUrl() {
        return properties.getProperty("db.JdbcUrl");
    }

    public String getJdbcUsername() {
        return properties.getProperty("db.JdbcUsername");
    }

    public String getJdbcPassword() {
        return properties.getProperty("db.JdbcPassword");
    }


}